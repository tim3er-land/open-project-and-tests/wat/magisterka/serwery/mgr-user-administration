package pl.wat.witowski.mgr.ms.mgruseradministration.service;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.dictionary.DictionaryCacheService;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.exceptions.ApiExceptions;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.RuleRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.UserRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.UserRoleRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserPasswordDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.dict.DictionaryItemDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.dict.UserTypeDictDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.security.jwt.SecurityUtils;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserService userService;
    private UserRepository userRepository;
    private RuleRepository rolesRepository;
    private UserRoleRepository userRoleRepository;
    private DictionaryCacheService dictionaryServices;

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        dictionaryServices = Mockito.mock(DictionaryCacheService.class);
        rolesRepository = Mockito.mock(RuleRepository.class);
        userRoleRepository = Mockito.mock(UserRoleRepository.class);
        userService = new UserService(userRepository, rolesRepository, dictionaryServices, userRoleRepository);
    }

    @Test
    void createUserNotFindDictTest() {
        try {
            when(dictionaryServices.getDictionaryItems(any())).thenThrow(new NullPointerException());
            userService.createUser(prepareUserDto(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-500-001-004", ap.getCode());
            Assert.assertEquals("mgr-user-administration-internal-error", ap.getDescription());
        }
    }

    @Test
    void createUserNotFindUserTypeTest() {
        when(dictionaryServices.getDictionaryItems(any())).thenReturn(prepreListDict());
        try {
            userService.createUser(prepareUserDto(), "USER_test");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-001-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-001-001", ap.getDescription());
        }
    }

    @Test
    void createUserCanNotCreateThisKingOfUserTest() {
        when(dictionaryServices.getDictionaryItems(any())).thenReturn(prepreListDict());
        when(dictionaryServices.convertJsonToList(any(), any())).thenReturn(UserTypeDictDto.builder()
                .canUserBeCreated(false)
                .userRole("USER")
                .build());
        try {
            userService.createUser(prepareUserDto(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-001-002", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-001-002", ap.getDescription());
        }
    }

    @Test
    void createUserValidationTest() {
        when(dictionaryServices.getDictionaryItems(any())).thenReturn(prepreListDict());
        when(dictionaryServices.convertJsonToList(any(), any())).thenReturn(UserTypeDictDto.builder()
                .canUserBeCreated(true)
                .userRole("USER")
                .build());
        try {
            userService.createUser(UserDto.builder().build(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-001-003", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-001-003", ap.getDescription());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-001-001")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-001-002")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-001-003")).findFirst().isPresent());
        }
    }

    @Test
    void createUserValidationLoginTakenTest() {
        when(userRepository.findUserByLogin(any())).thenReturn(new UsrUsersEntity());
        when(dictionaryServices.getDictionaryItems(any())).thenReturn(prepreListDict());
        when(dictionaryServices.convertJsonToList(any(), any())).thenReturn(UserTypeDictDto.builder()
                .canUserBeCreated(true)
                .userRole("USER")
                .build());
        try {
            userService.createUser(UserDto.builder().build(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-001-003", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-001-003", ap.getDescription());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-001-004")).findFirst().isPresent());
        }
    }

    @Test
    void getUserInfoTest() {
        try {
            when(userRepository.getUserInfo(any())).thenReturn(null);
            userService.getUserInfo();
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-500-002-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-internal-error", ap.getDescription());
        }
    }

    @Test
    void modifyUserValidationTest() {
        try {
            when(userRepository.findUserByLogin(any(), any())).thenReturn(new UsrUsersEntity());
            when(userRepository.getUserInfo(any())).thenReturn(null);
            userService.modifyUser(UserDto.builder().build());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-003-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-003-001", ap.getDescription());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-003-001")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-003-002")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-003-003")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-003-004")).findFirst().isPresent());
        }
    }

    @Test
    void changeUserPasswordExceptionTest() {
        try {
            when(userRepository.getUserInfo(any())).thenThrow(new NullPointerException());
            userService.changeUserPassoword(null);
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-500-004-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-internal-error", ap.getDescription());
        }
    }

    @Test
    void changeUserPasswordValidationTest() {
        try {
            UsrUsersEntity users = new UsrUsersEntity();
            users.setUsrPass(SecurityUtils.hashString("Password"));
            when(userRepository.getUserInfo(any())).thenReturn(users);
            userService.changeUserPassoword(UserPasswordDto.builder()
                    .newPassoword("Password").oldPassoword("password")
                    .build());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-004-002", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-004-002", ap.getDescription());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-004-002")).findFirst().isPresent());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-004-003")).findFirst().isPresent());
        }
    }

    @Test
    void changeUserPasswordValidation2Test() {
        try {
            when(userRepository.getUserInfo(any())).thenReturn(null);
            userService.changeUserPassoword(UserPasswordDto.builder()
                    .newPassoword("Password").oldPassoword("password")
                    .build());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-004-002", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-004-002", ap.getDescription());
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-user-administration-val-004-001")).findFirst().isPresent());
        }
    }

    @Test
    void deleteUserdExceptionTest() {
        try {
            when(userRepository.getUserInfo(any())).thenReturn(new UsrUsersEntity());
            when(userRepository.save(any())).thenReturn(new NullPointerException());
            userService.deleteUser();
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-500-005-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-internal-error", ap.getDescription());
        }
    }

    @Test
    void deleteUserdNotFindUserTest() {
        try {
            when(userRepository.getUserInfo(any())).thenReturn(null);
            userService.deleteUser();
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(HttpStatus.BAD_REQUEST, ap.getStatus());
            Assert.assertEquals("mgr-user-administration-400-005-001", ap.getCode());
            Assert.assertEquals("mgr-user-administration-400-005-001", ap.getDescription());
        }
    }

    private List<DictionaryItemDto> prepreListDict() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("canUserBeCreated", true);
        map.put("userRole", "USER");
        return Arrays.asList(DictionaryItemDto.builder()
                .itemCode("USER")
                .itemUid(UUID.randomUUID().toString())
                .itemJson(map)
                .build());
    }

    private UserDto prepareUserDto() {
        return UserDto.builder().login("pwitowski").email("piotr.witowski@student.wat.edu.pl").password("passoword").build();
    }
}
