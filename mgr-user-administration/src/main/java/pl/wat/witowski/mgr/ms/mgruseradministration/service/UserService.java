package pl.wat.witowski.mgr.ms.mgruseradministration.service;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.dictionary.DictionaryCacheService;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.exceptions.ApiExceptions;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrRolesEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersLogingHistoryEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersRolesEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.RuleRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.UserRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.repository.UserRoleRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserInfo;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserPasswordDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.dict.DictionaryItemDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.dict.UserTypeDictDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.security.jwt.JwtUtils;
import pl.wat.witowski.mgr.ms.mgruseradministration.security.jwt.KeyServiceUtils;
import pl.wat.witowski.mgr.ms.mgruseradministration.security.jwt.SecurityUtils;
import pl.wat.witowski.mgr.ms.mgruseradministration.validate.ChangeUserPasswordValidate;
import pl.wat.witowski.mgr.ms.mgruseradministration.validate.CreateUserValidate;
import pl.wat.witowski.mgr.ms.mgruseradministration.validate.ModifyUserValidate;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Service
public class UserService {

    private final UserRepository userRepository;
    private final RuleRepository rolesRepository;
    private final DictionaryCacheService dictionaryServices;
    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserService(UserRepository userRepository, RuleRepository rolesRepository, DictionaryCacheService dictionaryServices, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
        this.dictionaryServices = dictionaryServices;
        this.userRoleRepository = userRoleRepository;
    }


    public void createUser(UserDto userDto, String userType) {
        try {
            List<DictionaryItemDto> userTypes = dictionaryServices.getDictionaryItems("USER_TYPES");
            Optional<DictionaryItemDto> optionalDictionaryItemDto = userTypes.stream().filter(dictionaryItemDto -> dictionaryItemDto.getItemCode().equalsIgnoreCase(userType)).findFirst();
            UserTypeDictDto userTypeDictDto = null;
            if (!optionalDictionaryItemDto.isPresent()) {
                log.warn("Can not find user type: {} for create", userType);
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-user-administration-400-001-001", "mgr-user-administration-400-001-001");
            } else {
                userTypeDictDto = dictionaryServices.convertJsonToList(optionalDictionaryItemDto.get().getItemJson(), UserTypeDictDto.class);
                if (!userTypeDictDto.getCanUserBeCreated()) {
                    log.warn("user type: {} can not be created ", userType);
                    throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-user-administration-400-001-002", "mgr-user-administration-400-001-002");
                }
            }
            UsrUsersEntity userByLogin = userRepository.findUserByLogin(userDto.getLogin());
            new CreateUserValidate("mgr-user-administration-400-001-003", "mgr-user-administration-400-001-003", userDto, userByLogin).validate();
            UsrUsersEntity save = userRepository.save(prepareNewUserForSave(userDto, userType));
            UsrRolesEntity usrRolesEntity = rolesRepository.findByRlCode(userTypeDictDto.getUserRole());
            saveUserRole(save, usrRolesEntity);
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while createUser", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-user-administration-500-001-004", "mgr-user-administration-internal-error");
        }
    }

    private void saveUserRole(UsrUsersEntity save, UsrRolesEntity usrRolesEntity) {
        UsrUsersRolesEntity usrUsersRolesEntity = new UsrUsersRolesEntity();
        usrUsersRolesEntity.setUsrUsersByUsrUsrId(save);
        usrUsersRolesEntity.setUsrRolesByRlRlId(usrRolesEntity);
        usrUsersRolesEntity.setUsrRlAct((short) 1);
        userRoleRepository.save(usrUsersRolesEntity);
    }

    private UsrUsersEntity prepareNewUserForSave(UserDto userDto, String userType) {
        UsrUsersEntity usrUsersEntity = new UsrUsersEntity();
        usrUsersEntity.setUsrEmail(userDto.getEmail());
        usrUsersEntity.setUsrAct((short) 1);
        usrUsersEntity.setUsrErrorLogin(0);
        usrUsersEntity.setUsrUid(UUID.randomUUID().toString());
        usrUsersEntity.setUsrInsertBy(JwtUtils.getUUID(KeyServiceUtils.getMicroToken()));
        usrUsersEntity.setUsrLogin(userDto.getLogin().toUpperCase());
        usrUsersEntity.setUsrPass(SecurityUtils.hashString(userDto.getPassword()));
        usrUsersEntity.setUsrStatus((short) 1);
        usrUsersEntity.setUsrModifyBy(JwtUtils.getUUID(KeyServiceUtils.getMicroToken()));
        usrUsersEntity.setUsrUserTypeCode(userType.toUpperCase());
        usrUsersEntity.setUsrBlockDate(null);
        usrUsersEntity.setUsrInsertDate(LocalDateTime.now());
        usrUsersEntity.setUsrModifyDate(LocalDateTime.now());
        return usrUsersEntity;
    }

    public UserInfo getUserInfo() {
        try {
            UsrUsersEntity userInfo = userRepository.getUserInfo(JwtUtils.getUUID());
            Optional<UsrUsersLogingHistoryEntity> lastLoggin = userInfo.getUsrUsersLogingHistoriesByUsrId().stream()
                    .filter(usrUsersLogingHistoryEntity -> usrUsersLogingHistoryEntity.getUsrLogHisStatus().equalsIgnoreCase("SUCCESFUL")).sorted().findFirst();
            Optional<UsrUsersLogingHistoryEntity> lastLogginError = userInfo.getUsrUsersLogingHistoriesByUsrId().stream()
                    .filter(usrUsersLogingHistoryEntity -> usrUsersLogingHistoryEntity.getUsrLogHisStatus().equalsIgnoreCase("INCORECT PASSWORD")).sorted().findFirst();
            return UserInfo.builder()
                    .email(userInfo.getUsrEmail())
                    .login(userInfo.getUsrLogin())
                    .lastLogging(lastLoggin.isPresent() ? lastLoggin.get().getUsrLogHisData().toString() : null)
                    .lastErrorLogging(lastLogginError.isPresent() ? lastLogginError.get().getUsrLogHisData().toString() : null)
                    .build();
        } catch (Exception ex) {
            log.error("Error occured while getUserInfo", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-user-administration-500-002-001", "mgr-user-administration-internal-error");
        }
    }

    public UserDto modifyUser(UserDto userDto) {
        try {
            UsrUsersEntity userByLogin = userRepository.findUserByLogin(userDto.getLogin(), JwtUtils.getUUID());
            UsrUsersEntity users = userRepository.getUserInfo(JwtUtils.getUUID());
            new ModifyUserValidate("mgr-user-administration-400-003-001", "mgr-user-administration-400-003-001",
                    userDto, userByLogin, users).validate();

            UsrUsersEntity save = userRepository.save(prepareUserForUpdate(users, userDto));
            return UserDto.builder()
                    .email(save.getUsrEmail())
                    .login(save.getUsrLogin())
                    .build();
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while modifyUser", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-user-administration-500-003-002", "mgr-user-administration-internal-error");
        }
    }

    private UsrUsersEntity prepareUserForUpdate(UsrUsersEntity users, UserDto userDto) {
        users.setUsrModifyDate(LocalDateTime.now());
        users.setUsrModifyBy(JwtUtils.getUUID());
        users.setUsrLogin(userDto.getLogin());
        users.setUsrEmail(userDto.getEmail());
        return users;
    }

    public void changeUserPassoword(UserPasswordDto userPasswordDto) {
        try {
            UsrUsersEntity users = userRepository.getUserInfo(JwtUtils.getUUID());
            new ChangeUserPasswordValidate("mgr-user-administration-400-004-002", "mgr-user-administration-400-004-002",
                    userPasswordDto, users).validate();
            users.setUsrPass(SecurityUtils.hashString(userPasswordDto.getNewPassoword()));
            users.setUsrModifyBy(JwtUtils.getUUID());
            users.setUsrModifyDate(LocalDateTime.now());
            userRepository.save(users);
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while changeUserPassoword", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-user-administration-500-004-001", "mgr-user-administration-internal-error");
        }
    }

    public void deleteUser() {
        try {
            UsrUsersEntity users = userRepository.getUserInfo(JwtUtils.getUUID());
            if (users == null) {
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-user-administration-400-005-001", "mgr-user-administration-400-005-001");
            }
            users.setUsrStatus((short) 0);
            users.setUsrAct((short) 0);
            users.setUsrModifyBy(JwtUtils.getUUID());
            users.setUsrModifyDate(LocalDateTime.now());
            userRepository.save(users);
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while deleteUser", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-user-administration-500-005-001", "mgr-user-administration-internal-error");
        }
    }
}
