package pl.wat.witowski.mgr.ms.mgruseradministration.validate;

import org.springframework.util.StringUtils;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.BasicValidationServices;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserDto;

public class CreateUserValidate extends BasicValidationServices {
    private UserDto userDto;
    private UsrUsersEntity userByLogin;

    public CreateUserValidate(String errorCode, String errorDescription, UserDto userDto, UsrUsersEntity userByLogin) {
        super(errorCode, errorDescription);
        this.userDto = userDto;
        this.userByLogin = userByLogin;
    }

    @Override
    public void validate() {
        if (!StringUtils.hasText(userDto.getEmail())) {
            addNewErrorDto("email", "mgr-user-administration-val-001-001", "mgr-user-administration-val-001-001");
        }
        if (!StringUtils.hasText(userDto.getLogin())) {
            addNewErrorDto("login", "mgr-user-administration-val-001-002", "mgr-user-administration-val-001-002");
        }
        if (!StringUtils.hasText(userDto.getPassword())) {
            addNewErrorDto("password", "mgr-user-administration-val-001-003", "mgr-user-administration-val-001-003");
        }
        if (userByLogin != null) {
            addNewErrorDto("login", "mgr-user-administration-val-001-004", "mgr-user-administration-val-001-004");
        }
        super.validate();
    }
}
