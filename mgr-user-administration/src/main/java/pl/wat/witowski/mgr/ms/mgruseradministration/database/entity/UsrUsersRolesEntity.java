package pl.wat.witowski.mgr.ms.mgruseradministration.database.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "usr_users_roles", schema = "public", catalog = "postgres_mgr")
public class UsrUsersRolesEntity {

    @Id
    @Column(name = "usr_rl_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "usrUsersRolesIdSequence", sequenceName = "USR_USERS_ROLES_ID_SEQUENCE", allocationSize = 1)
    private long usrRlId;
    @Basic
    @Column(name = "usr_rl_act")
    private short usrRlAct;
    @ManyToOne
    @JoinColumn(name = "usr_usr_id", referencedColumnName = "usr_id", nullable = false)
    private UsrUsersEntity usrUsersByUsrUsrId;
    @ManyToOne
    @JoinColumn(name = "rl_rl_id", referencedColumnName = "rl_id", nullable = false)
    private UsrRolesEntity usrRolesByRlRlId;

    public long getUsrRlId() {
        return usrRlId;
    }

    public void setUsrRlId(long usrRlId) {
        this.usrRlId = usrRlId;
    }

    public short getUsrRlAct() {
        return usrRlAct;
    }

    public void setUsrRlAct(short usrRlAct) {
        this.usrRlAct = usrRlAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrUsersRolesEntity that = (UsrUsersRolesEntity) o;
        return usrRlId == that.usrRlId && usrRlAct == that.usrRlAct;
    }

    @Override
    public int hashCode() {
        return Objects.hash(usrRlId, usrRlAct);
    }

    public UsrUsersEntity getUsrUsersByUsrUsrId() {
        return usrUsersByUsrUsrId;
    }

    public void setUsrUsersByUsrUsrId(UsrUsersEntity usrUsersByUsrUsrId) {
        this.usrUsersByUsrUsrId = usrUsersByUsrUsrId;
    }

    public UsrRolesEntity getUsrRolesByRlRlId() {
        return usrRolesByRlRlId;
    }

    public void setUsrRolesByRlRlId(UsrRolesEntity usrRolesByRlRlId) {
        this.usrRolesByRlRlId = usrRolesByRlRlId;
    }
}
