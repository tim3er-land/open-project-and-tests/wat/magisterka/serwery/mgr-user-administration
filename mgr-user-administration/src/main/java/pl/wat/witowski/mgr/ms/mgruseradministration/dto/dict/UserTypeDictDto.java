package pl.wat.witowski.mgr.ms.mgruseradministration.dto.dict;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTypeDictDto {
    private Boolean canUserBeCreated;
    private String userRole;
}
