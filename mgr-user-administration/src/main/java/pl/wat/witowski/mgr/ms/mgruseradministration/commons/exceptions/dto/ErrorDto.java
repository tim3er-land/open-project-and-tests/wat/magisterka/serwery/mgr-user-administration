package pl.wat.witowski.mgr.ms.mgruseradministration.commons.exceptions.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonDeserialize(builder = ErrorDto.ErrorDtoBuilder.class)
public class ErrorDto {
    private String field;
    private String message;
    private String code;

    @JsonPOJOBuilder(withPrefix = "")
    public static class ErrorDtoBuilder {

    }
}
