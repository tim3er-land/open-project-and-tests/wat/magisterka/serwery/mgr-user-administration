package pl.wat.witowski.mgr.ms.mgruseradministration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgrUserAdministrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MgrUserAdministrationApplication.class, args);
    }

}
