package pl.wat.witowski.mgr.ms.mgruseradministration.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersLogingHistoryEntity;

public interface UserHistoryRepository extends JpaRepository<UsrUsersLogingHistoryEntity, Long> {

}
