package pl.wat.witowski.mgr.ms.mgruseradministration.validate;

import org.springframework.util.StringUtils;
import pl.wat.witowski.mgr.ms.mgruseradministration.commons.BasicValidationServices;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserDto;

public class ModifyUserValidate extends BasicValidationServices {
    private UserDto userDto;
    private UsrUsersEntity userByLogin;
    private UsrUsersEntity user;

    public ModifyUserValidate(String errorCode, String errorDescription, UserDto userDto, UsrUsersEntity userByLogin, UsrUsersEntity user) {
        super(errorCode, errorDescription);
        this.userDto = userDto;
        this.userByLogin = userByLogin;
        this.user = user;
    }

    @Override
    public void validate() {
        if (!StringUtils.hasText(userDto.getEmail())) {
            addNewErrorDto("email", "mgr-user-administration-val-003-001", "mgr-user-administration-val-003-001");
        }
        if (!StringUtils.hasText(userDto.getLogin())) {
            addNewErrorDto("login", "mgr-user-administration-val-003-002", "mgr-user-administration-val-003-002");
        }
        if (userByLogin != null) {
            addNewErrorDto("login", "mgr-user-administration-val-003-003", "mgr-user-administration-val-003-003");
        }
        if (user == null) {
            addNewErrorDto("user", "mgr-user-administration-val-003-004", "mgr-user-administration-val-003-004");
        }
        super.validate();
    }
}
