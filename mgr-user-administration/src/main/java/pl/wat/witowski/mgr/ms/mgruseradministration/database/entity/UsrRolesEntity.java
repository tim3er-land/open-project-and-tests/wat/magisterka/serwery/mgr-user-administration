package pl.wat.witowski.mgr.ms.mgruseradministration.database.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "usr_roles", schema = "public", catalog = "postgres_mgr")
public class UsrRolesEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "rl_id")
    private long rlId;
    @Basic
    @Column(name = "rl_code")
    private String rlCode;
    @Basic
    @Column(name = "rl_name")
    private String rlName;
    @Basic
    @Column(name = "rl_description")
    private String rlDescription;
    @Basic
    @Column(name = "rl_act")
    private short rlAct;

    public long getRlId() {
        return rlId;
    }

    public void setRlId(long rlId) {
        this.rlId = rlId;
    }

    public String getRlCode() {
        return rlCode;
    }

    public void setRlCode(String rlCode) {
        this.rlCode = rlCode;
    }

    public String getRlName() {
        return rlName;
    }

    public void setRlName(String rlName) {
        this.rlName = rlName;
    }

    public String getRlDescription() {
        return rlDescription;
    }

    public void setRlDescription(String rlDescription) {
        this.rlDescription = rlDescription;
    }

    public short getRlAct() {
        return rlAct;
    }

    public void setRlAct(short rlAct) {
        this.rlAct = rlAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrRolesEntity that = (UsrRolesEntity) o;
        return rlId == that.rlId && rlAct == that.rlAct && Objects.equals(rlCode, that.rlCode) && Objects.equals(rlName, that.rlName) && Objects.equals(rlDescription, that.rlDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rlId, rlCode, rlName, rlDescription, rlAct);
    }
}
