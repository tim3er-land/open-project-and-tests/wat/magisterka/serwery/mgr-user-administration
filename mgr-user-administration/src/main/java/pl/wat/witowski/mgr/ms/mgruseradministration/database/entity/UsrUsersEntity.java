package pl.wat.witowski.mgr.ms.mgruseradministration.database.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "usr_users", schema = "public", catalog = "postgres_mgr")
public class UsrUsersEntity {
    @Id
    @Column(name = "usr_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "usrUsersEntitySeq", sequenceName = "usr_user_id_sequence", allocationSize = 1)
    private long usrId;
    @Basic
    @Column(name = "usr_uid")
    private String usrUid;
    @Basic
    @Column(name = "usr_login")
    private String usrLogin;
    @Basic
    @Column(name = "usr_pass")
    private String usrPass;
    @Basic
    @Column(name = "usr_act")
    private short usrAct;
    @Basic
    @Column(name = "usr_status")
    private short usrStatus;
    @Basic
    @Column(name = "usr_user_type_code")
    private String usrUserTypeCode;
    @Basic
    @Column(name = "usr_insert_date")
    private LocalDateTime usrInsertDate;
    @Basic
    @Column(name = "usr_insert_by")
    private String usrInsertBy;
    @Basic
    @Column(name = "usr_modify_by")
    private String usrModifyBy;
    @Basic
    @Column(name = "usr_modify_date")
    private LocalDateTime usrModifyDate;
    @Basic
    @Column(name = "usr_block_date")
    private LocalDateTime usrBlockDate;
    @Basic
    @Column(name = "usr_error_login")
    private Integer usrErrorLogin;
    @Basic
    @Column(name = "usr_email")
    private String usrEmail;
    @OneToMany(mappedBy = "usrUsersByUsrUsrId")
    private Collection<UsrUsersLogingHistoryEntity> usrUsersLogingHistoriesByUsrId;

    public long getUsrId() {
        return usrId;
    }

    public void setUsrId(long usrId) {
        this.usrId = usrId;
    }

    public String getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(String usrUid) {
        this.usrUid = usrUid;
    }

    public String getUsrLogin() {
        return usrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        this.usrLogin = usrLogin;
    }

    public String getUsrPass() {
        return usrPass;
    }

    public void setUsrPass(String usrPass) {
        this.usrPass = usrPass;
    }

    public short getUsrAct() {
        return usrAct;
    }

    public void setUsrAct(short usrAct) {
        this.usrAct = usrAct;
    }

    public short getUsrStatus() {
        return usrStatus;
    }

    public void setUsrStatus(short usrStatus) {
        this.usrStatus = usrStatus;
    }

    public String getUsrUserTypeCode() {
        return usrUserTypeCode;
    }

    public void setUsrUserTypeCode(String usrUserTypeCode) {
        this.usrUserTypeCode = usrUserTypeCode;
    }

    public LocalDateTime getUsrInsertDate() {
        return usrInsertDate;
    }

    public void setUsrInsertDate(LocalDateTime usrInsertDate) {
        this.usrInsertDate = usrInsertDate;
    }

    public String getUsrInsertBy() {
        return usrInsertBy;
    }

    public void setUsrInsertBy(String usrInsertBy) {
        this.usrInsertBy = usrInsertBy;
    }

    public String getUsrModifyBy() {
        return usrModifyBy;
    }

    public void setUsrModifyBy(String usrModifyBy) {
        this.usrModifyBy = usrModifyBy;
    }

    public LocalDateTime getUsrModifyDate() {
        return usrModifyDate;
    }

    public void setUsrModifyDate(LocalDateTime usrModifyDate) {
        this.usrModifyDate = usrModifyDate;
    }

    public LocalDateTime getUsrBlockDate() {
        return usrBlockDate;
    }

    public void setUsrBlockDate(LocalDateTime usrBlockDate) {
        this.usrBlockDate = usrBlockDate;
    }

    public Integer getUsrErrorLogin() {
        return usrErrorLogin;
    }

    public void setUsrErrorLogin(Integer usrErrorLogin) {
        this.usrErrorLogin = usrErrorLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrUsersEntity users = (UsrUsersEntity) o;
        return usrId == users.usrId && usrAct == users.usrAct && usrStatus == users.usrStatus && Objects.equals(usrUid, users.usrUid) && Objects.equals(usrLogin, users.usrLogin) && Objects.equals(usrPass, users.usrPass) && Objects.equals(usrUserTypeCode, users.usrUserTypeCode) && Objects.equals(usrInsertDate, users.usrInsertDate) && Objects.equals(usrInsertBy, users.usrInsertBy) && Objects.equals(usrModifyBy, users.usrModifyBy) && Objects.equals(usrModifyDate, users.usrModifyDate) && Objects.equals(usrBlockDate, users.usrBlockDate) && Objects.equals(usrErrorLogin, users.usrErrorLogin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usrId, usrUid, usrLogin, usrPass, usrAct, usrStatus, usrUserTypeCode, usrInsertDate, usrInsertBy, usrModifyBy, usrModifyDate, usrBlockDate, usrErrorLogin);
    }

    public String getUsrEmail() {
        return usrEmail;
    }

    public void setUsrEmail(String usrEmail) {
        this.usrEmail = usrEmail;
    }

    public Collection<UsrUsersLogingHistoryEntity> getUsrUsersLogingHistoriesByUsrId() {
        return usrUsersLogingHistoriesByUsrId;
    }

    public void setUsrUsersLogingHistoriesByUsrId(Collection<UsrUsersLogingHistoryEntity> usrUsersLogingHistoriesByUsrId) {
        this.usrUsersLogingHistoriesByUsrId = usrUsersLogingHistoriesByUsrId;
    }
}
