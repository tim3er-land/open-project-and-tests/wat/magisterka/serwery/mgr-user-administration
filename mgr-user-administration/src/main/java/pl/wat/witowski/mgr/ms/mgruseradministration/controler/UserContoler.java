package pl.wat.witowski.mgr.ms.mgruseradministration.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserInfo;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserPasswordDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.service.UserService;

@CrossOrigin
@RestController
public class UserContoler {

    private final UserService userService;

    @Autowired
    public UserContoler(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/user")//001
    public ResponseEntity<Void> createUser(@RequestBody UserDto userDto, @RequestParam(required = true, defaultValue = "user") String userType) {
        userService.createUser(userDto, userType);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/user")//002
    public ResponseEntity<UserInfo> getUserInfo() {
        UserInfo userDto = userService.getUserInfo();
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping("/user")//003
    public ResponseEntity<UserDto> modifyUser(@RequestBody UserDto userDto) {
        userDto = userService.modifyUser(userDto);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping("/user/password")//004
    public ResponseEntity<Void> changeUserPassoword(@RequestBody UserPasswordDto userPasswordDto) {
        userService.changeUserPassoword(userPasswordDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/user")
    public ResponseEntity<Void> deleteUser() {
        userService.deleteUser();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
