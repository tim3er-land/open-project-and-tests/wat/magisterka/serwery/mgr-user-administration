package pl.wat.witowski.mgr.ms.mgruseradministration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {
    private String login;
    private String email;
    private String lastLogging;
    private String lastErrorLogging;
}
