package pl.wat.witowski.mgr.ms.mgruseradministration.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "usr_users_loging_history", schema = "public", catalog = "postgres_mgr")
public class UsrUsersLogingHistoryEntity implements Comparable<UsrUsersLogingHistoryEntity>{
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "usr_log_his_uid")
    private String usrLogHisUid;
    @Basic
    @Column(name = "usr_log_his_act")
    private short usrLogHisAct;
    @Basic
    @Column(name = "usr_log_his_data")
    private LocalDateTime usrLogHisData;
    @Basic
    @Column(name = "usr_log_his_ip")
    private String usrLogHisIp;
    @Basic
    @Column(name = "usr_log_his_location")
    private String usrLogHisLocation;
    @Basic
    @Column(name = "usr_log_his_status")
    private String usrLogHisStatus;
    @ManyToOne
    @JoinColumn(name = "usr_usr_id", referencedColumnName = "usr_id", nullable = false)
    private UsrUsersEntity usrUsersByUsrUsrId;

    public String getUsrLogHisUid() {
        return usrLogHisUid;
    }

    public void setUsrLogHisUid(String usrLogHisUid) {
        this.usrLogHisUid = usrLogHisUid;
    }

    public short getUsrLogHisAct() {
        return usrLogHisAct;
    }

    public void setUsrLogHisAct(short usrLogHisAct) {
        this.usrLogHisAct = usrLogHisAct;
    }

    public LocalDateTime getUsrLogHisData() {
        return usrLogHisData;
    }

    public void setUsrLogHisData(LocalDateTime usrLogHisData) {
        this.usrLogHisData = usrLogHisData;
    }

    public String getUsrLogHisIp() {
        return usrLogHisIp;
    }

    public void setUsrLogHisIp(String usrLogHisIp) {
        this.usrLogHisIp = usrLogHisIp;
    }

    public String getUsrLogHisLocation() {
        return usrLogHisLocation;
    }

    public void setUsrLogHisLocation(String usrLogHisLocation) {
        this.usrLogHisLocation = usrLogHisLocation;
    }

    public String getUsrLogHisStatus() {
        return usrLogHisStatus;
    }

    public void setUsrLogHisStatus(String usrLogHisStatus) {
        this.usrLogHisStatus = usrLogHisStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrUsersLogingHistoryEntity that = (UsrUsersLogingHistoryEntity) o;
        return  usrLogHisAct == that.usrLogHisAct && Objects.equals(usrLogHisUid, that.usrLogHisUid) && Objects.equals(usrLogHisData, that.usrLogHisData) && Objects.equals(usrLogHisIp, that.usrLogHisIp) && Objects.equals(usrLogHisLocation, that.usrLogHisLocation) && Objects.equals(usrLogHisStatus, that.usrLogHisStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usrLogHisUid, usrLogHisAct, usrLogHisData, usrLogHisIp, usrLogHisLocation, usrLogHisStatus);
    }

    public UsrUsersEntity getUsrUsersByUsrUsrId() {
        return usrUsersByUsrUsrId;
    }

    public void setUsrUsersByUsrUsrId(UsrUsersEntity usrUsersByUsrUsrId) {
        this.usrUsersByUsrUsrId = usrUsersByUsrUsrId;
    }


    @Override
    public int compareTo(UsrUsersLogingHistoryEntity o) {
        return o.usrLogHisData.compareTo(this.usrLogHisData);
    }
}
