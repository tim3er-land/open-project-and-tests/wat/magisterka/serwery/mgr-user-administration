package pl.wat.witowski.mgr.ms.mgruseradministration.commons.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HashMapLog {
    private String name;
    private String value;
}
