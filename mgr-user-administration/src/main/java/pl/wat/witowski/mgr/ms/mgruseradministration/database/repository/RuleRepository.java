package pl.wat.witowski.mgr.ms.mgruseradministration.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrRolesEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;

public interface RuleRepository extends JpaRepository<UsrRolesEntity, Long> {

    UsrRolesEntity findByRlCode(String userRole);
}
