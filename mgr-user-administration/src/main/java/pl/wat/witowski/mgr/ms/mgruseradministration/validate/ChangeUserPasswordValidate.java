package pl.wat.witowski.mgr.ms.mgruseradministration.validate;

import pl.wat.witowski.mgr.ms.mgruseradministration.commons.BasicValidationServices;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;
import pl.wat.witowski.mgr.ms.mgruseradministration.dto.UserPasswordDto;
import pl.wat.witowski.mgr.ms.mgruseradministration.security.jwt.SecurityUtils;

public class ChangeUserPasswordValidate extends BasicValidationServices {
    private UserPasswordDto userPasswordDto;
    private UsrUsersEntity user;

    public ChangeUserPasswordValidate(String errorCode, String errorDescription, UserPasswordDto userPasswordDto, UsrUsersEntity user) {
        super(errorCode, errorDescription);
        this.userPasswordDto = userPasswordDto;
        this.user = user;
    }

    @Override
    public void validate() {
        if (user == null) {
            addNewErrorDto("user", "mgr-user-administration-val-004-001", "mgr-user-administration-val-004-001");
        }
        if (user != null && !user.getUsrPass().equalsIgnoreCase(SecurityUtils.hashString(userPasswordDto.getOldPassoword()))) {
            addNewErrorDto("oldPassowrd", "mgr-user-administration-val-004-002", "mgr-user-administration-val-004-002");
        }
        if (user != null && user.getUsrPass().equalsIgnoreCase(SecurityUtils.hashString(userPasswordDto.getNewPassoword()))) {
            addNewErrorDto("newPassowrd", "mgr-user-administration-val-004-003", "mgr-user-administration-val-004-003");
        }
        super.validate();
    }
}
