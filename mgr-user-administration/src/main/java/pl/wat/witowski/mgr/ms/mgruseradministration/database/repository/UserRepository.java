package pl.wat.witowski.mgr.ms.mgruseradministration.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.witowski.mgr.ms.mgruseradministration.database.entity.UsrUsersEntity;

public interface UserRepository extends JpaRepository<UsrUsersEntity, Long> {


    @Query("select uue from UsrUsersEntity uue where uue.usrAct = 1 and uue.usrUid = :uuid")
    UsrUsersEntity getUserInfo(@Param("uuid") String uuid);


    @Query("select uue from UsrUsersEntity uue where uue.usrAct = 1 and uue.usrLogin = :login and uue.usrUid <> :uuid")
    UsrUsersEntity findUserByLogin(@Param("login") String login, @Param("uuid") String uuid);

    @Query("select uue from UsrUsersEntity uue where uue.usrAct = 1 and upper(uue.usrLogin) = upper(:login) ")
    UsrUsersEntity findUserByLogin(@Param("login") String login);
}
